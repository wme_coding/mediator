package tables;

import mediator.Mediator;
import mediator.OrderReceiver;

public class TableA extends Table{

    @Override
    void makeOrder() {
        mediator.receiveFromTableA(this);
    }
}
