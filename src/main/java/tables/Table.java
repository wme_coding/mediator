package tables;

import mediator.Mediator;
import mediator.OrderReceiver;

import java.util.ArrayList;
import java.util.List;

public abstract class Table {
    protected List<String> order;
    protected Mediator mediator;

    public Table() {
        this.order = new ArrayList<>();
        this.mediator = new OrderReceiver();
    }

    public void add(String order){
        this.order.add(order);
    }

    public void remove(String order){
        this.order.remove(order);
    }

    public List<String> getOrder() {
        return order;
    }

    abstract void makeOrder();
}
