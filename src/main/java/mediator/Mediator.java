package mediator;

import tables.*;

public interface Mediator {

    void receiveFromTableA(TableA tableA);
    void receiveFromTableB(TableB tableB);
    void receiveFromTableC(TableC tableC);
    void receiveFromTableD(TableD tableD);
    void receiveFromVipTableA(VipTableA vipTableA);
    void receiveFromVipTableB(VipTableB vipTableB);
}
