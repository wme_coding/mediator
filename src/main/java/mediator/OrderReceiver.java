package mediator;

import base.OrderStorage;
import order.Order;
import tables.*;

public class OrderReceiver implements Mediator {
    private OrderStorage orderStorage;

    public OrderReceiver() {
        this.orderStorage = new OrderStorage();
    }

    public void completeOrder(){
        orderStorage.completeOrder();
    }

    @Override
    public void receiveFromTableA(TableA tableA) {
        orderStorage.addOrder(new Order(tableA.getOrder(), "tableA", false));
    }

    @Override
    public void receiveFromTableB(TableB tableB) {
        orderStorage.addOrder(new Order(tableB.getOrder(), "tableB", false));
    }

    @Override
    public void receiveFromTableC(TableC tableC) {
        orderStorage.addOrder(new Order(tableC.getOrder(), "tableC", false));
    }

    @Override
    public void receiveFromTableD(TableD tableD) {
        orderStorage.addOrder(new Order(tableD.getOrder(), "tableD", false));
    }

    @Override
    public void receiveFromVipTableA(VipTableA vipTableA) {
        orderStorage.addOrder(new Order(vipTableA.getOrder(), "VipTableA", false));
    }

    @Override
    public void receiveFromVipTableB(VipTableB vipTableB) {
        orderStorage.addOrder(new Order(vipTableB.getOrder(), "VipTableB", false));
    }
}
