package order;

import java.util.List;

public class Order {
    private List<String> order;
    private String table;
    private boolean isVip;

    public Order(List<String> order, String table, boolean isVip) {
        this.order = order;
        this.table = table;
        this.isVip = isVip;
    }

    public void addToOrder(String order){
        this.order.add(order);
    }
}
