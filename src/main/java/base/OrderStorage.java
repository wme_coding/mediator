package base;

import order.Order;

import java.util.ArrayDeque;
import java.util.Queue;

public class OrderStorage {
    Queue<Order> orderQueue;

    public OrderStorage() {
        orderQueue = new ArrayDeque<>();
    }

    public void addOrder(Order order){
        orderQueue.add(order);
    }

    public void completeOrder(){
        orderQueue.remove();
    }
}
